/*
 * Conquest of Levidon
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

// Graph represented as list of neighbors for each vertex.
struct graph
{
	size_t count;
	struct vertex
	{
		size_t degree;
		struct edge
		{
			struct vertex *node;
			unsigned weight;
		} *neighbors;

		// Used for storing shortest path information.
		unsigned distance;
		struct vertex *neighbor;
		size_t position;
	} node[];
};

// Calculates distances and stores them in the graph.
int distances(struct graph *restrict graph, unsigned start);
