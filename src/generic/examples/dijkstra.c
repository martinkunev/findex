/*
 * Conquest of Levidon
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <limits.h>

#if defined(UNIT_TESTING)
# include <stdarg.h>
# include <stddef.h>
# include <setjmp.h>
# include <cmocka.h> /* libcmocka */
#endif

#include "dijkstra.h"

#define HEAP_TYPE struct vertex *
#define HEAP_ABOVE(a, b) ((a)->distance <= (b)->distance)
#define HEAP_UPDATE(h, i) ((h)->data[i]->position = (i))
#include "heap.g"

int distances(struct graph *restrict graph, unsigned start)
{
	struct heap heap;
	size_t i;

	struct vertex **nodes = malloc(graph->count * sizeof(*nodes));
	if (!nodes)
		return -1;

	for(i = 0; i < graph->count; i += 1)
	{
		nodes[i] = graph->node + i;
		graph->node[i].distance = UINT_MAX; // unreachable
		graph->node[i].position = i;
	}

	graph->node[start].distance = 0;
	graph->node[start].neighbor = graph->node + start;

	heap.data = nodes;
	heap.count = graph->count;

	heap_heapify(&heap);

	do
	{
		struct vertex *next = heap.data[0];

		if (next->distance == UINT_MAX)
			break;

		heap_pop(&heap);

		for(i = 0; i < next->degree; i += 1)
		{
			unsigned distance = next->distance + next->neighbors[i].weight;
			if (distance < next->neighbors[i].node->distance)
			{
				next->neighbors[i].node->distance = distance;
				next->neighbors[i].node->neighbor = next;
				heap_prioritize(&heap, next->neighbors[i].node->position);
			}
		}
	} while (heap.count > 1);

	free(nodes);
	return 0;
}

#if defined(UNIT_TESTING)
# define EDGE(t, w) (t), (w)

static void neighbors_set(struct vertex *restrict node, size_t degree, ...)
{
	va_list edges;
	size_t i;

	node->degree = degree;
	node->neighbors = malloc(node->degree * sizeof(*node->neighbors));
	if (!node->neighbors)
		abort();

	va_start(edges, degree);
	for(i = 0; i < degree; i += 1)
	{
		struct vertex *neighbor = va_arg(edges, struct vertex *);
		unsigned weight = va_arg(edges, unsigned);
		node->neighbors[i] = (struct edge){neighbor, weight};
	}
	va_end(edges);
}

static void test_dijkstra_small(void **state)
{
	struct graph *g = malloc(offsetof(struct graph, node) + 3 * sizeof(*g->node));

	if (!g)
		abort();

	g->count = 3;
	neighbors_set(g->node + 0, 1, EDGE(g->node + 1, 1));
	neighbors_set(g->node + 1, 2, EDGE(g->node + 0, 1), EDGE(g->node + 2, 2));
	neighbors_set(g->node + 2, 1, EDGE(g->node + 1, 2));

	distances(g, 0);
	assert_int_equal(g->node[0].distance, 0);
	assert_int_equal(g->node[1].distance, 1);
	assert_int_equal(g->node[2].distance, 3);

	distances(g, 1);
	assert_int_equal(g->node[0].distance, 1);
	assert_int_equal(g->node[1].distance, 0);
	assert_int_equal(g->node[2].distance, 2);

	distances(g, 2);
	assert_int_equal(g->node[0].distance, 3);
	assert_int_equal(g->node[1].distance, 2);
	assert_int_equal(g->node[2].distance, 0);

	free(g->node[2].neighbors);
	free(g->node[1].neighbors);
	free(g->node[0].neighbors);
	free(g);
}

static void test_dijkstra(void **state)
{
	struct graph *g = malloc(offsetof(struct graph, node) + 8 * sizeof(*g->node));

	if (!g)
		abort();

	g->count = 8;

	neighbors_set(g->node + 0, 2, EDGE(g->node + 1, 1), EDGE(g->node + 3, 2));
	neighbors_set(g->node + 1, 3, EDGE(g->node + 0, 1), EDGE(g->node + 2, 1), EDGE(g->node + 6, 4));
	neighbors_set(g->node + 2, 3, EDGE(g->node + 1, 1), EDGE(g->node + 5, 2), EDGE(g->node + 6, 3));
	neighbors_set(g->node + 3, 4, EDGE(g->node + 0, 2), EDGE(g->node + 4, 3), EDGE(g->node + 5, 2), EDGE(g->node + 6, 5));
	neighbors_set(g->node + 4, 3, EDGE(g->node + 3, 3), EDGE(g->node + 5, 2), EDGE(g->node + 7, 4));
	neighbors_set(g->node + 5, 3, EDGE(g->node + 2, 2), EDGE(g->node + 3, 2), EDGE(g->node + 4, 2));
	neighbors_set(g->node + 6, 4, EDGE(g->node + 1, 4), EDGE(g->node + 2, 3), EDGE(g->node + 3, 5), EDGE(g->node + 7, 3));
	neighbors_set(g->node + 7, 2, EDGE(g->node + 4, 4), EDGE(g->node + 6, 3));

	distances(g, 0);

	assert_int_equal(g->node[0].distance, 0);
	assert_int_equal(g->node[1].distance, 1);
	assert_int_equal(g->node[2].distance, 2);
	assert_int_equal(g->node[3].distance, 2);
	assert_int_equal(g->node[4].distance, 5);
	assert_int_equal(g->node[5].distance, 4);
	assert_int_equal(g->node[6].distance, 5);
	assert_int_equal(g->node[7].distance, 8);

	distances(g, 1);

	assert_int_equal(g->node[0].distance, 1);
	assert_int_equal(g->node[1].distance, 0);
	assert_int_equal(g->node[2].distance, 1);
	assert_int_equal(g->node[3].distance, 3);
	assert_int_equal(g->node[4].distance, 5);
	assert_int_equal(g->node[5].distance, 3);
	assert_int_equal(g->node[6].distance, 4);
	assert_int_equal(g->node[7].distance, 7);

	distances(g, 2);

	assert_int_equal(g->node[0].distance, 2);
	assert_int_equal(g->node[1].distance, 1);
	assert_int_equal(g->node[2].distance, 0);
	assert_int_equal(g->node[3].distance, 4);
	assert_int_equal(g->node[4].distance, 4);
	assert_int_equal(g->node[5].distance, 2);
	assert_int_equal(g->node[6].distance, 3);
	assert_int_equal(g->node[7].distance, 6);

	distances(g, 3);

	assert_int_equal(g->node[0].distance, 2);
	assert_int_equal(g->node[1].distance, 3);
	assert_int_equal(g->node[2].distance, 4);
	assert_int_equal(g->node[3].distance, 0);
	assert_int_equal(g->node[4].distance, 3);
	assert_int_equal(g->node[5].distance, 2);
	assert_int_equal(g->node[6].distance, 5);
	assert_int_equal(g->node[7].distance, 7);

	distances(g, 4);

	assert_int_equal(g->node[0].distance, 5);
	assert_int_equal(g->node[1].distance, 5);
	assert_int_equal(g->node[2].distance, 4);
	assert_int_equal(g->node[3].distance, 3);
	assert_int_equal(g->node[4].distance, 0);
	assert_int_equal(g->node[5].distance, 2);
	assert_int_equal(g->node[6].distance, 7);
	assert_int_equal(g->node[7].distance, 4);

	distances(g, 5);

	assert_int_equal(g->node[0].distance, 4);
	assert_int_equal(g->node[1].distance, 3);
	assert_int_equal(g->node[2].distance, 2);
	assert_int_equal(g->node[3].distance, 2);
	assert_int_equal(g->node[4].distance, 2);
	assert_int_equal(g->node[5].distance, 0);
	assert_int_equal(g->node[6].distance, 5);
	assert_int_equal(g->node[7].distance, 6);

	distances(g, 6);

	assert_int_equal(g->node[0].distance, 5);
	assert_int_equal(g->node[1].distance, 4);
	assert_int_equal(g->node[2].distance, 3);
	assert_int_equal(g->node[3].distance, 5);
	assert_int_equal(g->node[4].distance, 7);
	assert_int_equal(g->node[5].distance, 5);
	assert_int_equal(g->node[6].distance, 0);
	assert_int_equal(g->node[7].distance, 3);

	distances(g, 7);

	assert_int_equal(g->node[0].distance, 8);
	assert_int_equal(g->node[1].distance, 7);
	assert_int_equal(g->node[2].distance, 6);
	assert_int_equal(g->node[3].distance, 7);
	assert_int_equal(g->node[4].distance, 4);
	assert_int_equal(g->node[5].distance, 6);
	assert_int_equal(g->node[6].distance, 3);
	assert_int_equal(g->node[7].distance, 0);

	free(g->node[7].neighbors);
	free(g->node[6].neighbors);
	free(g->node[5].neighbors);
	free(g->node[4].neighbors);
	free(g->node[3].neighbors);
	free(g->node[2].neighbors);
	free(g->node[1].neighbors);
	free(g->node[0].neighbors);
	free(g);
}

int main()
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(test_dijkstra_small),
		cmocka_unit_test(test_dijkstra),
	};

	return cmocka_run_group_tests(tests, 0, 0);
}

# undef EDGE
#endif
