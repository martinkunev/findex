/*
 * Conquest of Levidon
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#if defined(UNIT_TESTING)
# include <stdarg.h>
# include <stddef.h>
# include <setjmp.h>
# include <cmocka.h> /* libcmocka */
#endif

#define HEAP_SOURCE
#include "sort.h"

// Sorts data in decending order.
void heap_sort(int *restrict data, size_t count)
{
	struct heap heap = {.data = data, .count = count};
    heap_heapify(&heap);
    while (heap.count)
    {
        int value = heap.data[0];
        heap_pop(&heap);
        heap.data[heap.count] = value;
    }
}

#if defined(UNIT_TESTING)

static void test_heap_sort(void **state)
{
	int data[32000];
	size_t i;

	srandom(1);
	for(i = 0; i < sizeof(data) / sizeof(*data); i += 1)
		data[i] = (int)random();

	heap_sort(data, sizeof(data) / sizeof(*data));

	for(i = 1; i < sizeof(data) / sizeof(*data); i += 1)
		assert_true(data[i - 1] >= data[i]);
}

int main()
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(test_heap_sort),
	};

	return cmocka_run_group_tests(tests, 0, 0);
}

#endif
