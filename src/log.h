/*
 * Filement Index
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Filement Index.
 *
 * Filement Index is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Filement Index is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Filement Index.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(NDEBUG)
# define LEVEL Info
#else
# define LEVEL Debug
#endif

// TODO trace documentation
// The highest 2 bits of the length store next argument's type.

#define _logs(s, l, ...)	((0x3L << (sizeof(size_t) * 8 - 2)) | (l)), (s)
#define logs(...)			_logs(__VA_ARGS__, sizeof(__VA_ARGS__) - 1)
#define logi(i)				(0x2L << (sizeof(size_t) * 8 - 2)), ((int64_t)(i))

void trace(int fd, ...);
#define trace(...) (trace)(__VA_ARGS__, (size_t)0)

// Use these to find logging location.
#if 0
# define _line_str_ex(l) #l
# define _line_str(l) _line_str_ex(l)
# define LOCATION logs(__FILE__ ":" _line_str(__LINE__) " "),
#else
#define LOCATION
#endif

#if defined(NDEBUG)
# define debug(...)
#else
# define debug(...) trace(2, LOCATION __VA_ARGS__)
#endif
#define warning(...) trace(2, LOCATION __VA_ARGS__)
#define error(...) trace(2, LOCATION logs("\\e[1;31mError:\\e[0m "), __VA_ARGS__)
