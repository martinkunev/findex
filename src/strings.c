/*
 * Filement Index
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Filement Index.
 *
 * Filement Index is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Filement Index is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Filement Index.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#if defined(UNIT_TESTING)
# include <stdarg.h>
# include <stddef.h>
# include <setjmp.h>
# include <cmocka.h> /* libcmocka */
#endif

#define ARRAY_SOURCE
#include "strings.h"

int string_append(struct string *restrict string, const char *data, size_t count)
{
	int status = string_expand(string, string->count + count);
	if (status < 0)
		return status;
	memcpy(string->data + string->count, data, count);
	string->count += count;
	return status;
}

#if defined(UNIT_TESTING)
# define APPEND(string, data) string_append((string), (data), sizeof(data) - 1)

static void test_array_expand(void **state)
{
	struct string s = {0};

	string_expand(&s, 12);

	assert_int_equal(s.count, 0);
	assert_int_equal(s.capacity, 16);
	assert_ptr_not_equal(s.data, 0);

	free(s.data);
}

static void test_array_stream(void **state)
{
	struct string s = {0};

	if (APPEND(&s, "hello") < 0)
		abort();
	if (APPEND(&s, " ") < 0)
		abort();
	if (APPEND(&s, "world") < 0)
		abort();
	if (APPEND(&s, "!") < 0)
		abort();

	assert_int_equal(s.count, 12);
	assert_memory_equal(s.data, "hello world!", 12);

	free(s.data);
}

int main()
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(test_array_expand),
		cmocka_unit_test(test_array_stream),
	};

	return cmocka_run_group_tests(tests, 0, 0);
}

# undef APPEND
#endif
